# Proyecto Final: VideoChat
- - - -
## Servidor

- Usando NodeJS y libreria PeerJS
- Basado en http://crosswalk-project.org/

### Instalar nodejs

`sudo apt-get install nodejs`

### Instalar npm

`sudo apt-get install npm`

### Instalar librerias usadas

```
#!bash

npm install peer
npm install ip
```

### Ejecutar servidor

`nodejs index.js`